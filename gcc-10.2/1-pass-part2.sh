
cd /



build_libstdc(){
cd /sources
cd gcc-10.2.0
rm -rf build
ln -s gthr-posix.h libgcc/gthr-default.h
mkdir -v build
cd build
../libstdc++-v3/configure \
	 CXXFLAGS="-g -O2 -D_GNU_SOURCE" \
	  --prefix=/usr \
	   --disable-multilib \
	    --disable-nls \
	     --host=$(uname -m)-lfs-linux-gnu \
	      --disable-libstdcxx-pch

make 
make install

}

build_gettext(){
cd /sources
tar -xvf gettext-0.21.tar.xz
cd gettext-0.21
./configure --disable-shared
make
cp -v gettext-tools/src/{msgfmt,msgmerge,xgettext} /usr/bin
}

build_bisson(){
cd /sources
tar -xvf bison-3.7.5.tar.xz
cd bison-3.7.5
./configure --prefix=/usr \
	 --docdir=/usr/share/doc/bison-3.7.5
make 
make install

}

build_perl(){
cd /sources
 tar -xvf perl-5.32.1.tar.xz
 cd perl-5.32.1

 sh Configure -des \
	  -Dprefix=/usr \
	   -Dvendorprefix=/usr \
	    -Dprivlib=/usr/lib/perl5/5.32/core_perl \
	     -Darchlib=/usr/lib/perl5/5.32/core_perl \
	      -Dsitelib=/usr/lib/perl5/5.32/site_perl \
	       -Dsitearch=/usr/lib/perl5/5.32/site_perl \
	        -Dvendorlib=/usr/lib/perl5/5.32/vendor_perl \
		 -Dvendorarch=/usr/lib/perl5/5.32/vendor_perl

 make 
 make install
}

build_python(){
cd /sources
tar -xvf Python-3.9.2.tar.xz
cd Python-3.9.2
./configure --prefix=/usr \
	 --enable-shared \
	  --without-ensurepip
make
make install

}

build_texinfo(){
cd /sources
tar -xvf texinfo-6.7.tar.xz
cd texinfo-6.7
./configure --prefix=/usr

make
make install
}

build_util(){

mkdir -pv /var/lib/hwclock
cd /sources

tar -xvf util-linux-2.36.2.tar.xz
cd util-linux-2.36.2

 sed -i -e 's/undef HAVE_PRLIMIT/define HAVE_PRLIMIT/g' config.h.in
 sed -i -e 's/undef HAVE_SETNS/define HAVE_SETNS/g'  config.h.in
 sed -i -e 's/undef HAVE_UNSHARE/define HAVE_UNSHARE/g'  config.h.in

./configure ADJTIME_PATH=/var/lib/hwclock/adjtime \
	 --docdir=/usr/share/doc/util-linux-2.36.2 \
	  --disable-chfn-chsh \
	   --disable-login \
	    --disable-nologin \
	     --disable-su \
	      --disable-setpriv \
	       --disable-runuser \
	        --disable-pylibmount \
		 --disable-static \
		  --without-python \
		   runstatedir=/run


make

make install

}

build_libstdc
build_gettext
build_bisson
build_perl
build_python
build_texinfo
build_util
