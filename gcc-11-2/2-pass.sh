
cd /tmp


build_man_pages(){
cd /sources/
tar -xvf man-pages-5.10.tar.xz
cd man-pages-5.10
make install
}


build_iana(){
cd /sources/
tar -xvf iana-etc-20210202.tar.gz
cd iana-etc-20210202

cp services protocols /etc
}

build_glibc(){
cd /sources/
tar -xvf glibc-2.33.tar.xz
cd glibc-2.33
patch -Np1 -i ../glibc-2.33-fhs-1.patch
sed -e '402a\ *result = local->data.services[database_index];' \
	-i nss/nss_database.c
mkdir -v build
cd build


../configure --prefix=/usr \
	--disable-werror \
	--enable-kernel=3.2 \
	--enable-stack-protector=strong \
	--with-headers=/usr/include \
	libc_cv_slibdir=/lib

make
touch /etc/ld.so.conf
sed '/test-installation/s@$(PERL)@echo not running@' -i ../Makefile
make install
cp -v ../nscd/nscd.conf /etc/nscd.conf
mkdir -pv /var/cache/nscd

mkdir -pv /usr/lib/locale
localedef -i POSIX -f UTF-8 C.UTF-8 2> /dev/null || true
localedef -i cs_CZ -f UTF-8 cs_CZ.UTF-8
localedef -i de_DE -f ISO-8859-1 de_DE
localedef -i de_DE@euro -f ISO-8859-15 de_DE@euro
localedef -i de_DE -f UTF-8 de_DE.UTF-8
localedef -i el_GR -f ISO-8859-7 el_GR
localedef -i en_GB -f UTF-8 en_GB.UTF-8
localedef -i en_HK -f ISO-8859-1 en_HK
localedef -i en_PH -f ISO-8859-1 en_PH
localedef -i en_US -f ISO-8859-1 en_US
localedef -i en_US -f UTF-8 en_US.UTF-8
localedef -i es_MX -f ISO-8859-1 es_MX
localedef -i fa_IR -f UTF-8 fa_IR
localedef -i fr_FR -f ISO-8859-1 fr_FR
localedef -i fr_FR@euro -f ISO-8859-15 fr_FR@euro
localedef -i fr_FR -f UTF-8 fr_FR.UTF-8
localedef -i it_IT -f ISO-8859-1 it_IT
localedef -i it_IT -f UTF-8 it_IT.UTF-8
localedef -i ja_JP -f EUC-JP ja_JP
localedef -i ja_JP -f SHIFT_JIS ja_JP.SIJS 2> /dev/null || true
localedef -i ja_JP -f UTF-8 ja_JP.UTF-8
localedef -i ru_RU -f KOI8-R ru_RU.KOI8-R
localedef -i ru_RU -f UTF-8 ru_RU.UTF-8
localedef -i tr_TR -f UTF-8 tr_TR.UTF-8
localedef -i zh_CN -f GB18030 zh_CN.GB18030
localedef -i zh_HK -f BIG5-HKSCS zh_HK.BIG5-HKSCS
make localedata/install-locales



cat > /etc/nsswitch.conf << "EOF"
# Begin /etc/nsswitch.conf
passwd: files
group: files
shadow: files
hosts: files dns
networks: files
protocols: files
services: files
ethers: files
rpc: files
# End /etc/nsswitch.conf
EOF

tar -xf ../../tzdata2021a.tar.gz
ZONEINFO=/usr/share/zoneinfo
mkdir -pv $ZONEINFO/{posix,right}
for tz in etcetera southamerica northamerica europe africa antarctica \
	asia australasia backward; do
zic -L /dev/null -d $ZONEINFO ${tz}
zic -L /dev/null -d $ZONEINFO/posix ${tz}
zic -L leapseconds -d $ZONEINFO/right ${tz}
done
cp -v zone.tab zone1970.tab iso3166.tab $ZONEINFO
zic -d $ZONEINFO -p America/New_York
unset ZONEINFO

 ln -sfv /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

cat > /etc/ld.so.conf << "EOF"
# Begin /etc/ld.so.conf
/usr/local/lib
/opt/lib
EOF

cat >> /etc/ld.so.conf << "EOF"
# Add an include directory
include /etc/ld.so.conf.d/*.conf
EOF
mkdir -pv /etc/ld.so.conf.d

}

build_zlib(){
 cd /sources/
 tar -xvf zlib-1.2.11.tar.xz
 cd zlib-1.2.11

 ./configure --prefix=/usr
 make
 make install
mv -v /usr/lib/libz.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libz.so) /usr/lib/libz.so

rm -fv /usr/lib/libz.a
}

build_bzip2(){
cd /sources
tar -xvf bzip2-1.0.8.tar.gz
cd  bzip2-1.0.8

patch -Np1 -i ../bzip2-1.0.8-install_docs-1.patch
sed -i 's@\(ln -s -f \)$(PREFIX)/bin/@\1@' Makefile
sed -i "s@(PREFIX)/man@(PREFIX)/share/man@g" Makefile

make -f Makefile-libbz2_so

make clean

make
make PREFIX=/usr install

cp -v bzip2-shared /bin/bzip2
cp -av libbz2.so* /lib
ln -sv ../../lib/libbz2.so.1.0 /usr/lib/libbz2.so
rm -v /usr/bin/{bunzip2,bzcat,bzip2}
ln -sv bzip2 /bin/bunzip2
ln -sv bzip2 /bin/bzcat

}

buid_xz()
{
tar -xvf xz-5.2.5.tar.xz
cd xz-5.2.5
./configure --prefix=/usr \
	--disable-static \
	--docdir=/usr/share/doc/xz-5.2.5
make

make install
mv -v /usr/bin/{lzma,unlzma,lzcat,xz,unxz,xzcat} /bin
mv -v /usr/lib/liblzma.so.* /lib
ln -svf ../../lib/$(readlink /usr/lib/liblzma.so) /usr/lib/liblzma.so

}

build_zstd()
{
cd /sources/
tar -xvf zstd-1.4.8.tar.gz
cd  zstd-1.4.8

make
make prefix=/usr install
rm -v /usr/lib/libzstd.a
mv -v /usr/lib/libzstd.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libzstd.so) /usr/lib/libzstd.so

}

build_file(){
cd /sources/
tar -xvf file-5.39.tar.gz
cd file-5.39
./configure --prefix=/usr
make
make install


}

build_readline(){
cd /sources/
tar -xvf readline-8.1.tar.gz
cd readline-8.1
sed -i '/MV.*old/d' Makefile.in
sed -i '/{OLDSUFF}/c:' support/shlib-install
./configure --prefix=/usr \
	--disable-static \
	--with-curses \
	--docdir=/usr/share/doc/readline-8.1
make SHLIB_LIBS="-lncursesw"
make SHLIB_LIBS="-lncursesw" install

mv -v /usr/lib/lib{readline,history}.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libreadline.so) /usr/lib/libreadline.so
ln -sfv ../../lib/$(readlink /usr/lib/libhistory.so ) /usr/lib/libhistory.so

install -v -m644 doc/*.{ps,pdf,html,dvi} /usr/share/doc/readline-8.1


}
build_m4(){
cd /sources/
tar -xvf m4-1.4.18.tar.xz
cd  m4-1.4.18
sed -i 's/IO_ftrylockfile/IO_EOF_SEEN/' lib/*.c
echo "#define _IO_IN_BACKUP 0x100" >> lib/stdio-impl.h
./configure --prefix=/usr
make
make install

}

build_bc(){
cd /sources/
tar -xvf bc-3.3.0.tar.xz
cd bc-3.3.0
PREFIX=/usr CC=gcc ./configure.sh -G -O3

make
make install
}

build_flex(){
cd /sources/
tar -xvf flex-2.6.4.tar.gz
cd  flex-2.6.4
./configure --prefix=/usr \
	--docdir=/usr/share/doc/flex-2.6.4 \
	--disable-static
make 
make install
ln -sv flex /usr/bin/lex
}

build_tcl(){
cd /sources/
tar -xvf tcl8.6.11-src.tar.gz
cd tcl8.6.11
tar -xf ../tcl8.6.11-html.tar.gz --strip-components=1
SRCDIR=$(pwd)
cd unix
./configure --prefix=/usr \
	--mandir=/usr/share/man \
	$([ "$(uname -m)" = x86_64 ] && echo --enable-64bit)
make
sed -e "s|$SRCDIR/unix|/usr/lib|" \
	-e "s|$SRCDIR|/usr/include|" \
	-i tclConfig.sh
sed -e "s|$SRCDIR/unix/pkgs/tdbc1.1.2|/usr/lib/tdbc1.1.2|" \
	-e "s|$SRCDIR/pkgs/tdbc1.1.2/generic|/usr/include|" \
	-e "s|$SRCDIR/pkgs/tdbc1.1.2/library|/usr/lib/tcl8.6|" \
	-e "s|$SRCDIR/pkgs/tdbc1.1.2|/usr/include|" \
	-i pkgs/tdbc1.1.2/tdbcConfig.sh
sed -e "s|$SRCDIR/unix/pkgs/itcl4.2.1|/usr/lib/itcl4.2.1|" \
	-e "s|$SRCDIR/pkgs/itcl4.2.1/generic|/usr/include|" \
	-e "s|$SRCDIR/pkgs/itcl4.2.1|/usr/include|" \
	-i pkgs/itcl4.2.1/itclConfig.sh
unset SRCDIR
make install
chmod -v u+w /usr/lib/libtcl8.6.so
make install-private-headers
ln -sfv tclsh8.6 /usr/bin/tclsh
mv /usr/share/man/man3/{Thread,Tcl_Thread}.3

}

build_expect()
{
cd /sources/
tar -xvf expect5.45.4.tar.gz
cd expect5.45.4

./configure --prefix=/usr \
	--with-tcl=/usr/lib \
	--enable-shared \
	--mandir=/usr/share/man \
	--with-tclinclude=/usr/include

sed -i -e 's/-ldl  -lm \\/-ldl  -lm -lz -lpthread -lutil\\/g' Makefile
make
make install
ln -svf expect5.45.4/libexpect5.45.4.so /usr/lib
}

build_dejagnu(){
cd /sources/
tar -xvf dejagnu-1.6.2.tar.gz
cd dejagnu-1.6.2
./configure --prefix=/usr
makeinfo --html --no-split -o doc/dejagnu.html doc/dejagnu.texi
makeinfo --plaintext -o doc/dejagnu.txt doc/dejagnu.texi
make
make install
install -v -dm755 /usr/share/doc/dejagnu-1.6.2
install -v -m644 doc/dejagnu.{html,txt} /usr/share/doc/dejagnu-1.6.2

}

build_binutils(){
cd /sources/
tar -xvf binutils-2.36.1.tar.xz
cd  binutils-2.36.1

sed -i '/@\tincremental_copy/d' gold/testsuite/Makefile.in

mkdir -v build
cd build


../configure --prefix=/usr \
	--enable-gold \
	--enable-ld=default \
	--enable-plugins \
	--enable-shared \
	--disable-werror \
	--enable-64-bit-bfd \
	--with-system-zlib

make tooldir=/usr
#make -k check

make tooldir=/usr install

rm -fv /usr/lib/lib{bfd,ctf,ctf-nobfd,opcodes}.a

}

build_gmp(){
cd /sources/
 tar -xvf gmp-6.2.1.tar.xz
 cd gmp-6.2.1
 ./configure --prefix=/usr \
	 --enable-cxx \
	 --disable-static \
	 --docdir=/usr/share/doc/gmp-6.2.1

 make
 make html
 
#make check 2>&1 | tee gmp-check-log
#awk '/# PASS:/{total+=$3} ; END{print total}' gmp-check-log

 make install
 make install-html


}

build_mpfr(){
cd /sources/
tar -xvf mpfr-4.1.0.tar.xz
cd mpfr-4.1.0
./configure --prefix=/usr \
	--disable-static \
	--enable-thread-safe \
	--docdir=/usr/share/doc/mpfr-4.1.0
make
make html
#make check
make install
make install-html


}

build_mpc()
{
cd /sources/
tar -xvf mpc-1.2.1.tar.gz
cd mpc-1.2.1

./configure --prefix=/usr \
	--disable-static \
	--docdir=/usr/share/doc/mpc-1.2.1
make
make html
#make check
make install
make install-html

}

build_attr(){
cd /sources/
tar -xvf attr-2.4.48.tar.gz
cd  attr-2.4.48
./configure --prefix=/usr \
	--bindir=/bin \
	--disable-static \
	--sysconfdir=/etc \
	--docdir=/usr/share/doc/attr-2.4.48
make 
make install
mv -v /usr/lib/libattr.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libattr.so) /usr/lib/libattr.so

}

build_acl(){
cd /sources/
tar -xvf acl-2.2.53.tar.gz
cd acl-2.2.53
./configure --prefix=/usr \
	--bindir=/bin \
	--disable-static \
	--libexecdir=/usr/lib \
	--docdir=/usr/share/doc/acl-2.2.53
make 
make install
mv -v /usr/lib/libacl.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libacl.so) /usr/lib/libacl.so

}

build_cap(){
cd /sources/
tar -xvf libcap-2.48.tar.xz
cd libcap-2.48
sed -i '/install -m.*STA/d' libcap/Makefile
make prefix=/usr lib=lib

#make test

make prefix=/usr lib=lib install
for libname in cap psx; do
	mv -v /usr/lib/lib${libname}.so.* /lib
	ln -sfv ../../lib/lib${libname}.so.2 /usr/lib/lib${libname}.so
	chmod -v 755 /lib/lib${libname}.so.2.48
done

}
build_shadow(){
cd /sources/
tar -xvf shadow-4.8.1.tar.xz
cd shadow-4.8.1
sed -i 's/groups$(EXEEXT) //' src/Makefile.in
find man -name Makefile.in -exec sed -i 's/groups\.1 / /' {} \;
find man -name Makefile.in -exec sed -i 's/getspnam\.3 / /' {} \;
find man -name Makefile.in -exec sed -i 's/passwd\.5 / /' {} \;

sed -e 's:#ENCRYPT_METHOD DES:ENCRYPT_METHOD SHA512:' \
	-e 's:/var/spool/mail:/var/mail:' \
	-i etc/login.defs

sed -i 's/1000/999/' etc/useradd
touch /usr/bin/passwd
./configure --sysconfdir=/etc \
	--with-group-name-max-length=32
make
make install
pwconv
grpconv

sed -i 's/yes/no/' /etc/default/useradd
}

build_gcc(){
cd /sources/ 
tar -xvf gcc-11.2.0.tar.xz
cd  gcc-11.2.0

case $(uname -m) in
	x86_64)
		sed -e '/m64=/s/lib64/lib/' \
			-i.orig gcc/config/i386/t-linux64
		;;
esac
mkdir -v build
cd build

../configure --prefix=/usr \
	LD=ld \
	--enable-languages=c,c++ \
	--disable-multilib \
	--disable-bootstrap \
	--with-system-zlib
make
ulimit -s 32768
#chown -Rv tester .
#su tester -c "PATH=$PATH make -k check"
#../contrib/test_summary

make install
rm -rf /usr/lib/gcc/$(gcc -dumpmachine)/11.2.0/include-fixed/bits/
chown -v -R root:root \
	/usr/lib/gcc/*linux-gnu/11.2.0/include{,-fixed}
ln -sv ../usr/bin/cpp /lib
ln -sfv ../../libexec/gcc/$(gcc -dumpmachine)/11.2.0/liblto_plugin.so \
	/usr/lib/bfd-plugins/
echo 'int main(){}' > dummy.c
cc dummy.c -v -Wl,--verbose &> dummy.log
readelf -l a.out | grep ': /lib'
grep -o '/usr/lib.*/crt[1in].*succeeded' dummy.log
grep -B4 '^ /usr/include' dummy.log
grep 'SEARCH.*/usr/lib' dummy.log |sed 's|; |\n|g'
grep "/lib.*/libc.so.6 " dummy.log
grep found dummy.log
rm -v dummy.c a.out dummy.log
mkdir -pv /usr/share/gdb/auto-load/usr/lib
mv -v /usr/lib/*gdb.py /usr/share/gdb/auto-load/usr/lib




}

build_pkgconfig(){
cd /sources/
tar -xvf pkg-config-0.29.2.tar.gz
cd pkg-config-0.29.2

./configure --prefix=/usr \
	 --with-internal-glib \
	  --disable-host-tool \
	   --docdir=/usr/share/doc/pkg-config-0.29.2
make
#make check
make install

}

build_ncurses(){
cd /sources/
tar -xvf ncurses-6.2.tar.gz
cd  ncurses-6.2

./configure --prefix=/usr \
	 --mandir=/usr/share/man \
	  --with-shared \
	   --without-debug \
	    --without-normal \
	     --enable-pc-files \
	      --enable-widec

make
make install

mv -v /usr/lib/libncursesw.so.6* /lib

ln -sfv ../../lib/$(readlink /usr/lib/libncursesw.so) /usr/lib/libncursesw.so

for lib in ncurses form panel menu ; do
	 rm -vf /usr/lib/lib${lib}.so
	  echo "INPUT(-l${lib}w)" > /usr/lib/lib${lib}.so
	   ln -sfv ${lib}w.pc /usr/lib/pkgconfig/${lib}.pc
done

rm -vf /usr/lib/libcursesw.so
echo "INPUT(-lncursesw)" > /usr/lib/libcursesw.so
ln -sfv libncurses.so /usr/lib/libcurses.so

rm -fv /usr/lib/libncurses++w.a

mkdir -v /usr/share/doc/ncurses-6.2
cp -v -R doc/* /usr/share/doc/ncurses-6.2
}

build_sed(){
cd /sources/
 tar -xvf sed-4.8.tar.xz

 cd  sed-4.8
 
 ./configure --prefix=/usr --bindir=/bin

 make
 make html
 #chown -Rv tester .
 #su tester -c "PATH=$PATH make check"


 make install
 install -d -m755 /usr/share/doc/sed-4.8
 install -m644 doc/sed.html /usr/share/doc/sed-4.8
}


build_psmisc(){
	cd /sources/
tar -xvf psmisc-23.4.tar.xz
cd psmisc-23.4
./configure --prefix=/usr
make
make install

mv -v /usr/bin/fuser /bin
mv -v /usr/bin/killall /bin

}

build_gettext(){
cd /sources/
tar -xvf gettext-0.21.tar.xz
cd gettext-0.21

./configure --prefix=/usr \
	 --disable-static \
	  --docdir=/usr/share/doc/gettext-0.21

make 
#make check
make install
chmod -v 0755 /usr/lib/preloadable_libintl.so


}

build_bison(){
cd /sources/
tar -xvf bison-3.7.5.tar.xz
cd bison-3.7.5
./configure --prefix=/usr --docdir=/usr/share/doc/bison-3.7.5
make
#make check
make install
}

build_grep(){
cd /sources/
tar -xvf grep-3.6.tar.xz
cd grep-3.6
./configure --prefix=/usr --bindir=/bin
make
#make check
make install

}

build_bash(){
cd /sources/
tar -xvf bash-5.1.tar.gz
cd bash-5.1

sed -i '/^bashline.o:.*shmbchar.h/a bashline.o: ${DEFDIR}/builtext.h' Makefile.in

./configure --prefix=/usr \
	 --docdir=/usr/share/doc/bash-5.1 \
	  --without-bash-malloc \
	   --with-installed-readline

make

#chown -Rv tester .
#su tester << EOF
#PATH=$PATH make tests < $(tty)
#EOF

make install
mv -vf /usr/bin/bash /bin


}

build_libtool(){
cd /sources/

 tar -xvf libtool-2.4.6.tar.xz
 cd  libtool-2.4.6

 ./configure --prefix=/usr

 make 
# make check
 make install

 rm -fv /usr/lib/libltdl.a

}

build_gdbm(){
cd /sources/

tar -xvf gdbm-1.19.tar.gz
cd gdbm-1.19
./configure --prefix=/usr \
	 --disable-static \
	  --enable-libgdbm-compat
make 
#make check
make install
}

build_gperf(){
cd /sources/
tar -xvf gperf-3.1.tar.gz
cd gperf-3.1
./configure --prefix=/usr --docdir=/usr/share/doc/gperf-3.1
make 
#make -j1 check
make install


	
}

build_expat(){
cd /sources/
tar -xvf expat-2.2.10.tar.xz
cd expat-2.2.10
./configure --prefix=/usr \
	 --disable-static \
	  --docdir=/usr/share/doc/expat-2.2.10
make 
#make check
make install
install -v -m644 doc/*.{html,png,css} /usr/share/doc/expat-2.2.10


}
build_inetutils(){
cd /sources/
tar -xvf inetutils-2.0.tar.xz
cd inetutils-2.0

./configure --prefix=/usr \
	 --localstatedir=/var \
	  --disable-logger \
	   --disable-whois \
	    --disable-rcp \
	     --disable-rexec \
	      --disable-rlogin \
	       --disable-rsh \
	        --disable-servers

make
#make check
make install

mv -v /usr/bin/{hostname,ping,ping6,traceroute} /bin
mv -v /usr/bin/ifconfig /sbin

}
build_perl(){
	cd  /sources/
tar -xvf perl-5.32.1.tar.xz
cd perl-5.32.1
export BUILD_ZLIB=False
export BUILD_BZIP2=0

sh Configure -des \
	 -Dprefix=/usr \
	  -Dvendorprefix=/usr \
	   -Dprivlib=/usr/lib/perl5/5.32/core_perl \
	    -Darchlib=/usr/lib/perl5/5.32/core_perl \
	     -Dsitelib=/usr/lib/perl5/5.32/site_perl \
	      -Dsitearch=/usr/lib/perl5/5.32/site_perl \
	       -Dvendorlib=/usr/lib/perl5/5.32/vendor_perl \
	        -Dvendorarch=/usr/lib/perl5/5.32/vendor_perl \
		 -Dman1dir=/usr/share/man/man1 \
		  -Dman3dir=/usr/share/man/man3 \
		   -Dpager="/usr/bin/less -isR" \
		    -Duseshrplib \
		     -Dusethreads

make

#make test
make install
unset BUILD_ZLIB BUILD_BZIP2


}

build_xmlparser(){
cd /sources/
tar -xvf XML-Parser-2.46.tar.gz
cd XML-Parser-2.46
perl Makefile.PL

make 
#make test
make install
}

built_intltool(){
cd /sources/

tar -xvf intltool-0.51.0.tar.gz

cd intltool-0.51.0
sed -i 's:\\\${:\\\$\\{:' intltool-update.in

./configure --prefix=/usr
make 
#make check
make install
install -v -Dm644 doc/I18N-HOWTO /usr/share/doc/intltool-0.51.0/I18N-HOWTO

}

build_autoconf(){
cd /sources/
 tar -xvf autoconf-2.71.tar.xz
 cd  autoconf-2.71
 ./configure --prefix=/usr
 make
# make check
 make install

}

build_automake(){
cd /sources/
 tar -xvf automake-1.16.3.tar.xz

 cd  automake-1.16.3
 sed -i "s/''/etags/" t/tags-lisp-space.sh
 ./configure --prefix=/usr --docdir=/usr/share/doc/automake-1.16.3

 make

# make -j4 check
 make install
}

build_kmod(){
cd /sources/
tar -xvf  kmod-28.tar.xz
cd kmod-28
./configure --prefix=/usr \
	--bindir=/bin \
	--sysconfdir=/etc \
	--with-rootlibdir=/lib \
	--with-xz \
	--with-zstd \
	--with-zlib
make

make install
for target in depmod insmod lsmod modinfo modprobe rmmod; do
	ln -sfv ../bin/kmod /sbin/$target
done
ln -sfv kmod /bin/lsmod

}

build_elfutils(){
cd /sources/

tar -xvf elfutils-0.183.tar.bz2
cd elfutils-0.183

./configure --prefix=/usr \
	--disable-debuginfod \
	--enable-libdebuginfod=dummy \
	--libdir=/lib

make
#make check
make -C libelf install
install -vm644 config/libelf.pc /usr/lib/pkgconfig
rm /lib/libelf.a

}

build_libffi(){
cd /sources
tar -xvf libffi-3.3.tar.gz
cd libffi-3.3

./configure --prefix=/usr --disable-static --with-gcc-arch=native

make 
#make check 
make install

}

build_openssl(){
cd /sources
tar -xvf openssl-1.1.1k.tar.gz
cd openssl-1.1.1k

./config --prefix=/usr \
	--openssldir=/etc/ssl \
	--libdir=lib \
	shared \
	zlib-dynamic
make
#make test
sed -i '/INSTALL_LIBS/s/libcrypto.a libssl.a//' Makefile
make MANSUFFIX=ssl install

mv -v /usr/share/doc/openssl /usr/share/doc/openssl-1.1.1k
cp -vfr doc/* /usr/share/doc/openssl-1.1.1k

}

build_python(){
cd /sources/
tar -xvf Python-3.9.2.tar.xz
cd  Python-3.9.2
./configure --prefix=/usr \
	--enable-shared \
	--with-system-expat \
	--with-system-ffi \
	--with-ensurepip=yes
make

#make test
make install

install -v -dm755 /usr/share/doc/python-3.9.2/html
tar --strip-components=1 \
	--no-same-owner \
	--no-same-permissions \
	-C /usr/share/doc/python-3.9.2/html \
	-xvf ../python-3.9.2-docs-html.tar.bz2


}




build_ninja(){
cd /sources
tar -xvf ninja-1.10.2.tar.gz
cd ninja-1.10.2

export NINJAJOBS=4


sed -i '/int Guess/a \
	int j = 0;\
	char* jobs = getenv( "NINJAJOBS" );\
	if ( jobs != NULL ) j = atoi( jobs );\
		if ( j > 0 ) return j;\
			' src/ninja.cc

python3 configure.py --bootstrap

./ninja ninja_test
./ninja_test --gtest_filter=-SubprocessTest.SetWithLots

install -vm755 ninja /usr/bin/
install -vDm644 misc/bash-completion /usr/share/bash-completion/completions/ninja
install -vDm644 misc/zsh-completion /usr/share/zsh/site-functions/_ninja

}

build_meson(){
cd /sources/
 tar -xvf meson-0.57.1.tar.gz
 cd   meson-0.57.1
 python3 setup.py build
 python3 setup.py install --root=dest
 cp -rv dest/* /

}


build_coreutils(){
	cd /sources/
 tar -xvf coreutils-8.32.tar.xz
 cd  coreutils-8.32
 patch -Np1 -i ../coreutils-8.32-i18n-1.patch
 sed -i '/test.lock/s/^/#/' gnulib-tests/gnulib.mk
 autoreconf -fiv
 FORCE_UNSAFE_CONFIGURE=1 ./configure \
	 --prefix=/usr \
	 --enable-no-install-program=kill,uptime
 make
 
#make NON_ROOT_USERNAME=tester check-root
#echo "dummy:x:102:tester" >> /etc/group
#chown -Rv tester .
#su tester -c "PATH=$PATH make RUN_EXPENSIVE_TESTS=yes check"
#sed -i '/dummy/d' /etc/group

make install
mv -v /usr/bin/{cat,chgrp,chmod,chown,cp,date,dd,df,echo} /bin
mv -v /usr/bin/{false,ln,ls,mkdir,mknod,mv,pwd,rm} /bin
mv -v /usr/bin/{rmdir,stty,sync,true,uname} /bin
mv -v /usr/bin/chroot /usr/sbin
mv -v /usr/share/man/man1/chroot.1 /usr/share/man/man8/chroot.8
sed -i 's/"1"/"8"/' /usr/share/man/man8/chroot.8
mv -v /usr/bin/{head,nice,sleep,touch} /bin

}

build_check(){
cd /sources/

tar -xvf check-0.15.2.tar.gz
cd check-0.15.2
./configure --prefix=/usr --disable-static
make
#make check

make docdir=/usr/share/doc/check-0.15.2 install



}

build_diff(){
cd /sources/
 tar -xvf diffutils-3.7.tar.xz
 cd  diffutils-3.7
 ./configure --prefix=/usr
 make
# make check
 make install

}

build_gawk(){
cd /sources/

 tar -xvf gawk-5.1.0.tar.xz
 cd  gawk-5.1.0
 sed -i 's/extras//' Makefile.in
 ./configure --prefix=/usr
 make
#make check
 make install
 mkdir -v /usr/share/doc/gawk-5.1.0
 cp -v doc/{awkforai.txt,*.{eps,pdf,jpg}} /usr/share/doc/gawk-5.1.0

}

build_findutils(){
cd /sources


tar -xvf findutils-4.8.0.tar.xz
cd findutils-4.8.0

./configure --prefix=/usr --localstatedir=/var/lib/locate

make

#chown -Rv tester .
#su tester -c "PATH=$PATH make check"

make install

mv -v /usr/bin/find /bin
sed -i 's|find:=${BINDIR}|find:=/bin|' /usr/bin/updatedb


}

build_groff(){
cd /sources
tar -xvf groff-1.22.4.tar.gz
cd groff-1.22.4

PAGE=A4 ./configure --prefix=/usr

make -j1

make install
}

build_grub(){
cd /sources/

tar -xvf grub-2.04.tar.xz
cd grub-2.04

sed "s/gold-version/& -R .note.gnu.property/" \
	-i Makefile.in grub-core/Makefile.in

./configure --prefix=/usr \
	--sbindir=/sbin \
	--sysconfdir=/etc \
	--disable-efiemu \
	--disable-werror

make

make install
mv -v /etc/bash_completion.d/grub /usr/share/bash-completion/completions

}

build_less(){
cd /sources/
tar -xvf less-563.tar.gz
cd less-563
./configure --prefix=/usr --sysconfdir=/etc
make 
make install

}

build_gzip(){
cd /sources/
tar -xvf  gzip-1.10.tar.xz
cd gzip-1.10

./configure --prefix=/usr

make 
#make check
make install
mv -v /usr/bin/gzip /bin


}

build_iproute(){
cd /sources
 tar -xvf iproute2-5.10.0.tar.xz

 cd  iproute2-5.10.0

 sed -i /ARPD/d Makefile
 rm -fv man/man8/arpd.8

 sed -i 's/.m_ipt.o//' tc/Makefile

 make 
 make DOCDIR=/usr/share/doc/iproute2-5.10.0 install

}

build_kbd(){
cd /sources
tar -xvf kbd-2.4.0.tar.xz
cd  kbd-2.4.0
patch -Np1 -i ../kbd-2.4.0-backspace-1.patch

sed -i '/RESIZECONS_PROGS=/s/yes/no/' configure
sed -i 's/resizecons.8 //' docs/man/man8/Makefile.in

./configure --prefix=/usr --disable-vlock

make
#make check

make install
mkdir -v /usr/share/doc/kbd-2.4.0
cp -R -v docs/doc/* /usr/share/doc/kbd-2.4.0


}

build_pipe(){
cd /sources
tar -xvf libpipeline-1.5.3.tar.gz
cd libpipeline-1.5.3

./configure --prefix=/usr

make
#make check
make install

}

build_make(){
 cd /sources 
tar -xvf make-4.3.tar.gz
 cd make-4.3

 ./configure --prefix=/usr
 make

 #make check 
 make install

}

build_patch(){
cd /sources
tar -xvf patch-2.7.6.tar.xz
cd patch-2.7.6
./configure --prefix=/usr
make
#make check
make install


}

build_mandb(){
cd /sources
tar -xvf man-db-2.9.4.tar.xz

cd man-db-2.9.4

./configure --prefix=/usr \
	--docdir=/usr/share/doc/man-db-2.9.4 \
	--sysconfdir=/etc \
	--disable-setuid \
	--enable-cache-owner=bin \
	--with-browser=/usr/bin/lynx \
	--with-vgrind=/usr/bin/vgrind \
	--with-grap=/usr/bin/grap \
	--with-systemdtmpfilesdir= \
	--with-systemdsystemunitdir=
make

#make check
make install


}

build_tar(){

cd /sources

tar -xvf tar-1.34.tar.xz
cd tar-1.34
FORCE_UNSAFE_CONFIGURE=1 \
	./configure --prefix=/usr \
	--bindir=/bin
make
#make check
make install
make -C doc install-html docdir=/usr/share/doc/tar-1.34


}

build_texinfo(){
cd /sources
tar -xvf texinfo-6.7.tar.xz
cd  texinfo-6.7

./configure --prefix=/usr
make
#make check
make install

make TEXMF=/usr/share/texmf install-tex

pushd /usr/share/info
rm -v dir
for f in *
do install-info $f dir 2>/dev/null
done
popd

}

build_vim(){
cd /sources
tar -xvf vim-8.2.2433.tar.gz
cd  vim-8.2.2433
echo '#define SYS_VIMRC_FILE "/etc/vimrc"' >> src/feature.h
./configure --prefix=/usr
#chown -Rv tester .
#u tester -c "LANG=en_US.UTF-8 make -j1 test" &> vim-test.log
make install
ln -sv vim /usr/bin/vi
for L in /usr/share/man/{,*/}man1/vim.1; do
	ln -sv vim.1 $(dirname $L)/vi.1
done
ln -sv ../vim/vim82/doc /usr/share/doc/vim-8.2.2433


cat > /etc/vimrc << "EOF"
" Begin /etc/vimrc
" Ensure defaults are set before customizing settings, not after
source $VIMRUNTIME/defaults.vim
let skip_defaults_vim=1
set nocompatible
set backspace=2
set mouse=
syntax on
if (&term == "xterm") || (&term == "putty")
	set background=dark
endif
" End /etc/vimrc
EOF




}

build_eudev(){

cd /sources
tar -xvf eudev-3.2.10.tar.gz
cd  eudev-3.2.10

./configure --prefix=/usr \
	--bindir=/sbin \
	--sbindir=/sbin \
	--libdir=/usr/lib \
	--sysconfdir=/etc \
	--libexecdir=/lib \
	--with-rootprefix= \
	--with-rootlibdir=/lib \
	--enable-manpages \
	--disable-static
make

mkdir -pv /lib/udev/rules.d
mkdir -pv /etc/udev/rules.d

#make check
make install

tar -xvf ../udev-lfs-20171102.tar.xz
make -f udev-lfs-20171102/Makefile.lfs install

udevadm hwdb --update

}

build_procps(){



cd /sources
 tar -xvf procps-ng-3.3.17.tar.xz
 cd  procps-3.3.17

 ./configure --prefix=/usr \
	 --exec-prefix= \
	 --libdir=/usr/lib \
	 --docdir=/usr/share/doc/procps-ng-3.3.17 \
	 --disable-static \
	 --disable-kill

make

#make_check

make install

mv -v /usr/lib/libprocps.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libprocps.so) /usr/lib/libprocps.so



}

function build_util(){
cd /sources

tar -xvf util-linux-2.36.2.tar.xz
cd util-linux-2.36.2

./configure ADJTIME_PATH=/var/lib/hwclock/adjtime \
	--docdir=/usr/share/doc/util-linux-2.36.2 \
	--disable-chfn-chsh \
	--disable-login \
	--disable-nologin \
	--disable-su \
	--disable-setpriv \
	--disable-runuser \
	--disable-pylibmount \
	--disable-static \
	--without-python \
	--without-systemd \
	--without-systemdsystemunitdir \
	runstatedir=/run

make 

#chown -Rv tester .
#su tester -c "make -k check"

make install

}
build_e2fs(){
cd /sources

tar -xvf e2fsprogs-1.46.1.tar.gz
cd  e2fsprogs-1.46.1

mkdir -v build
cd build

../configure --prefix=/usr \
	--bindir=/bin \
	--with-root-prefix="" \
	--enable-elf-shlibs \
	--disable-libblkid \
	--disable-libuuid \
	--disable-uuidd \
	--disable-fsck


make 
#make check
make install
rm -fv /usr/lib/{libcom_err,libe2p,libext2fs,libss}.a
gunzip -v /usr/share/info/libext2fs.info.gz
install-info --dir-file=/usr/share/info/dir /usr/share/info/libext2fs.info
makeinfo -o doc/com_err.info ../lib/et/com_err.texinfo
install -v -m644 doc/com_err.info /usr/share/info
install-info --dir-file=/usr/share/info/dir /usr/share/info/com_err.info

}

build_syslog(){


cd /sources

 tar -xvf sysklogd-1.5.1.tar.gz
 cd  sysklogd-1.5.1
 sed -i '/Error loading kernel symbols/{n;n;d}' ksym_mod.c
 sed -i 's/union wait/int/' syslogd.c
 make

 make BINDIR=/sbin install

cat > /etc/syslog.conf << "EOF"
# Begin /etc/syslog.conf
auth,authpriv.* -/var/log/auth.log
*.*;auth,authpriv.none -/var/log/sys.log
daemon.* -/var/log/daemon.log
kern.* -/var/log/kern.log
mail.* -/var/log/mail.log
user.* -/var/log/user.log
*.emerg *
# End /etc/syslog.conf
EOF

}

build_sysinit(){
cd /sources

 tar -xvf sysvinit-2.98.tar.xz
 cd sysvinit-2.98

 patch -Np1 -i ../sysvinit-2.98-consolidated-1.patch

 make
 make install

}

#build_man_pages
#build_iana
#build_glibc
#build_zlib
#build_bzip2
#buid_xz
#build_zstd
#build_file
#build_readline
#build_m4
#build_bc
#build_flex
#build_tcl
#build_expect
#build_dejagnu
#build_binutils
build_gmp
build_mpfr
build_mpc
build_attr
build_acl
build_cap
build_shadow
build_gcc
build_pkgconfig
build_ncurses
build_sed
build_psmisc
build_gettext
build_bison
build_grep
build_bash
build_libtool
build_gdbm
build_gperf
build_expat
build_inetutils
build_perl
build_xmlparser
built_intltool
build_autoconf
build_automake
build_kmod
build_elfutils
build_libffi
build_openssl
build_python
build_ninja
build_meson
build_coreutils
build_check
build_diff
build_gawk
build_findutils
build_groff
build_grub
build_less
build_gzip
build_iproute
build_kbd
build_pipe
build_make
build_mandb
build_tar
build_texinfo
build_vim
build_eudev
build_procps
build_util
build_e2fs
build_syslog
build_sysinit
#





